<footer class="site-footer">
    <div class="container">

        <div class="l-site-footer">
            <div class="l-site-footer-col l-site-footer-col1">
                <div class="site-footer__content">
                    <p class="copyright">&copy; Copyright <?php echo date('Y'); ?> Wandle</p>
                    <p>Design by <a href="http://broadgatecreative.co.uk">Broadgate Creative</a></p>
                </div>
            </div>
            <div class="l-site-footer-col l-site-footer-col2">
                <a href="#home" class="js-smooth-scroll footer__btn">Return to top</a>
                <a href="https://www.wandle.com/" class="footer__btn" target="_blank">Wandle.com</a>
            </div>
        </div>

    </div>
</footer>