<header id="home" class="site-header">
    <div class="container container--relative">

        <div class="site-logo">
            <a class="site-logo__brand" href="/" title="Big Clean Switch">
                <img src="/assets/images/logos/logo-wandle-report.png" alt="Wandle Report 2016" />
            </a>
       </div>

        <ul class="social">
            <li><a href="https://www.facebook.com/wandleHA/" title="facebook" target="_blank"><i class="icon--social icon--facebook"></i></a></li>
            <li><a href="https://twitter.com/WandleHA" title="twitter" target="_blank"><i class="icon--social icon--twitter"></i></a></li>
            <li><a href="https://www.linkedin.com/company/wandle-housing-association" title="linkedin" target="_blank"><i class="icon--social icon--linkedin"></i></a></li>
        </ul>

        <div class="main-navigation">
            <a class="js-menu-link nav-cta">
                Menu
                <span class="lines-button x">
                    <span class="lines"></span>
                </span>
            </a>
            <ul class="js-menu-items nav-primary">
                <li class="menu-item"><a class="js-smooth-scroll" href="#welcome">Welcome</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#purpose">Our Purpose, Vision And Values</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#our-homes">Our Homes</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#in-numbers">2018/2019 In Numbers</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#our-corporate-plan">Our Corporate Plan</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#value-for-money">Value For Money</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#improving-customer-experience">Improving Customer Experience</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#building-new-homes">Building New Homes</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#investing-in-communities">Investing In Communities</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#investing-in-homes">Investing In Homes</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#investing-in-repairs">Investing In Repairs</a></li>
                <li class="menu-item"><a class="js-smooth-scroll" href="#our-people">Our People</a></li>                
                <li class="menu-item"><a class="js-smooth-scroll" href="#strengthening-our-governance">Strengthening Our Governance</a></li>
            </ul>
        </div>

    </div>
</header>