<section id="our-people" class="js-waypoint7 section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--blue-bright">
            Our People
            <div class="toggle">
                <a class="js-smooth-scroll" href="#investing-in-repairs"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#strengthening-our-governance"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">Our People Strategy aims to support the organisation to deliver on its commitments and recognises the role our people skills, knowledge, experience and attitudes play in achieving its mission and vision.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <img src="/assets/images/sections/14-Our-People2.jpg" alt="Programme graduates holding their certificates" />
                            <a class="js-zoom-trigger image__zoom image__zoom--blue-bright"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--blue-bright">
                            Our staff receive a wide range of benefits and training opportunities.
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/14-Our-People2.jpg" alt="Programme graduates holding their certificates" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--blue-bright"><i class="icon icon--x"></i></a>
                            <figcaption class="js-zoom-trigger image__caption image__caption--large image__caption--blue-bright">
                                Our staff receive a wide range of benefits and training opportunities.
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section__inner section__inner--space">
        <div class="container">
            <div class="l-col4">
                <div class="l-col4-col l-col4-col1">

                    <div class="js-waypoint7-up animation-fade block block--blue-bright block--light-text block--min-height">
                        <h3 class="block__title block__title--people">The Wandle Way - a culture for success</h3>
                        <p class="block__desc">Embedding our culture is critical to providing direction to our people and ensuring that they behave in a way that is congruent to the Wandle Way.</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col2">

                    <div class="js-waypoint7-up animation-fade animated--delay-02s block block--blue-bright block--light-text block--min-height">
                        <h3 class="block__title block__title--people">Driving Wandle – leading the way to success</h3>
                        <p class="block__desc">This year we’ve taken a fresh approach to training, focusing more on the day-to-day skills our staff tell us they need.</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col4">

                    <div class="js-waypoint7-up animation-fade animated--delay-04s block block--blue-bright block--light-text block--min-height">
                        <h3 class="block__title block__title--people">Our People are Wandle – investing in our people</h3>
                        <p class="block__desc">We believe that our people make a difference. We want to continually improve employee engagement, recognising that a highly engaged workforce will deliver excellent services for our customers.</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col3">

                    <div class="js-waypoint7-up animation-fade animated--delay-04s block block--blue-bright block--light-text block--min-height">
                        <h3 class="block__title block__title--people">Working for Wandle – Our offer</h3>
                        <p class="block__desc">We want to attract, retain and develop our people and recognise that the offer we provide must be flexible to ensure our people feel valued for their contribution to Wandle.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>