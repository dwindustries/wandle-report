<section id="investing-in-homes" class="js-waypoint5 section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--pea">
            Investing In Homes
            <div class="toggle">
                <a class="js-smooth-scroll" href="#investing-in-communities"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#investing-in-repairs"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">We invest in our properties to ensure that they are well managed, good quality homes that residents want to live in. Through maintenance work, and by making improvements, we provide our residents with homes that are modern, warm and safe.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <img src="/assets/images/sections/12-Wandle-Investing-in-Homes.jpg" alt="Builder installing tiles on a roof" />
                            <a class="js-zoom-trigger image__zoom image__zoom--pea"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--pea">
                            We carried out gas safety checks at 100% of our properties
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/12-Wandle-Investing-in-Homes.jpg" alt="Builder installing tiles on a roof" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--pea"><i class="icon icon--x"></i></a>
                            <figcaption class="js-zoom-trigger image__caption image__caption--large image__caption--pea">
                                We carried out gas safety checks at 100% of our properties
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section__inner section__inner--space">
        <div class="container">
            <div class="l-col4">
                <div class="l-col4-col l-col4-col1">

                    <div class="js-waypoint5-up animation-fade block block--compact">
                        <img class="block__img" src="/assets/images/homes/paint-roller.svg" />
                        <p class="block__desc">We carried out programmed work externally to 163 properties – including 137 window window replacements, 135 door replacements, 16 roofs and 64 external decorations.</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col2">

                    <div class="js-waypoint5-up animation-fade animated--delay-02s block block--compact">
                        <img class="block__img" src="/assets/images/homes/wrench.svg" />
                        <p class="block__desc">We installed 105 new kitchens and 40 new bathrooms</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col3">

                    <div class="js-waypoint5-up animation-fade animated--delay-04s block block--compact">
                        <img class="block__img" src="/assets/images/homes/drill.svg" />                        
                        <p class="block__desc">We carried out gas safety checks at 100% of our properties.</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col4">

                    <div class="js-waypoint5-up animation-fade animated--delay-06s block block--compact">
                        <img class="block__img" src="/assets/images/homes/door.svg" />                        
                        <p class="block__desc">We carried out internal decoration, flooring and lighting works in 57 blocks.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>