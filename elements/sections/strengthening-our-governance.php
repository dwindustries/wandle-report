<section id="strengthening-our-governance" class="js-waypoint8 section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--pink">
            Strengthening Our Governance
            <div class="toggle">
                <a class="js-smooth-scroll" href="#our-people"><i class="icon icon--arrow icon--arrow-up"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">During 2018/19, the Regulator for Social Housing conducted a focussed In-Depth Assessment, and changed our governance rating from G1 to G2 – which remains a compliant rating.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <img src="/assets/images/sections/15-Strengthening-our-Gorvernance.jpg" alt="Tour of a development model" />
                            <a class="js-zoom-trigger image__zoom image__zoom--pink"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--pink">
                            Having the right skills on board is crucial to our success.
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/15-Strengthening-our-Gorvernance.jpg" alt="Tour of a development model" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--pink"><i class="icon icon--x"></i></a>
                            <figcaption class="js-zoom-trigger image__caption image__caption--large image__caption--pink">
                                Having the right skills on board is crucial to our success.
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section__inner section__inner--space">
        <div class="container">
            <div class="l-col4">
                <div class="l-col4-col l-col4-col1">

                    <div class="js-waypoint8-up animation-fade block block--governance">
                        <h3 class="block__title block__title--pink">In-Depth Assessment</h3>
                        <p class="block__desc">Our periodic In-Depth Assessment happened in Summer 2018.</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col2">

                    <div class="js-waypoint8-up animation-fade animated--delay-02s block block--governance">
                        <h3 class="block__title block__title--pink">Action Plan</h3>
                        <p class="block__desc">We have, and continue to work closely with the Regulator to progress our action plan and have completed a number of these.</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col3">

                    <div class="js-waypoint8-up animation-fade animated--delay-04s block block--governance">
                        <h3 class="block__title block__title--pink">Golden Rules</h3>
                        <p class="block__desc">Our compliance with our golden rules and covenants is dependent on robust financial management, and we have put improved financial analysis and reporting in place during 2018/19 to support these areas.</p>
                    </div>

                </div>
                <div class="l-col4-col l-col4-col4">

                    <div class="js-waypoint8-up animation-fade animated--delay-06s block block--governance">
                        <h3 class="block__title block__title--pink">G1</h3>
                        <p class="block__desc">The Board are committed to returning to the top rating for governance as soon as possible.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>