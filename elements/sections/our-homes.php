<section id="our-homes" class="section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--navy">
            Our Homes
            <div class="toggle">
                <a class="js-smooth-scroll" href="#purpose"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#in-numbers"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">We manage over 7,000 homes across nine south London boroughs.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <img src="/assets/images/sections/Wandle-Cotton-apartments-external.jpg" alt="Cotton Apartments" />
                            <a class="js-zoom-trigger image__zoom image__zoom--navy"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--navy">
                            43 new shared-owners now call the Cotton Apartments, in Wandsworth, home.
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/Wandle-Cotton-apartments-external.jpg" alt="Cotton Apartments" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--navy"><i class="icon icon--x"></i></a>
                            <figcaption class="image__caption image__caption--large image__caption--navy">
                                43 new shared-owners now call the Cotton Apartments, in Wandsworth, home.
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section__inner section__inner--space section__inner--navy">
        <div class="container">
            <div class="regions">
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title">Bromley</h5>
                        <img class="region__symbol region__symbol--bromley" src="assets/images/regions/bromley.svg" />
                        <p class="js-counter region__number">82</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title region__title--no-space">Croydon</h5>
                        <img class="region__symbol region__symbol--croydon" src="assets/images/regions/croydon.svg" />
                        <p class="js-counter region__number">1,360</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title region__title--no-space">Kingston upon Thames</h5>
                        <img class="region__symbol region__symbol--kingston-upon-thames" src="assets/images/regions/kingston-upon-thames.svg" />
                        <p class="js-counter region__number">135</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title">Lambeth</h5>
                        <img class="region__symbol region__symbol--lambeth" src="assets/images/regions/lambeth.svg" />
                        <p class="js-counter region__number">727</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title">Lewisham</h5>
                        <img class="region__symbol region__symbol--lewisham" src="assets/images/regions/lewisham.svg" />
                        <p class="js-counter region__number">315</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title">Merton</h5>
                        <img class="region__symbol region__symbol--merton" src="assets/images/regions/merton.svg" />
                        <p class="js-counter region__number">1,438</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title">Southwark</h5>
                        <img class="region__symbol region__symbol--southwark" src="assets/images/regions/southwark.svg" />
                        <p class="js-counter region__number">1,420</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title">Sutton</h5>
                        <img class="region__symbol region__symbol--sutton" src="assets/images/regions/sutton.svg" />
                        <p class="js-counter region__number">263</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title">Wandsworth</h5>
                        <img class="region__symbol region__symbol--wandsworth" src="assets/images/regions/wandsworth.svg" />
                        <p class="js-counter region__number">1,804</p>
                    </div>
                </div>
                <div class="l-region">
                    <div class="region">
                        <h5 class="region__title region__title--no-space">Other Boroughs</h5>
                        <img class="region__symbol region__symbol--other" src="assets/images/regions/other-boroughs.svg" />
                        <p class="js-counter region__number">55</p>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="section--divider"></div>
</section>