<section class="section section--border section--home section--dark-grey">

    <div class="split-container split-container--left split-container--home split-container--home-left"></div>
    <div class="split-container split-container--right split-container--home split-container--home-right"></div>

    <div class="container">

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <div class="site-intro">
                    <h1 class="animated animated--delay-06s fadeInDown site-intro__title"><strong>Annual Report</strong> <br />2018/19</h1>
                    <p class="animated animated--delay-06s fadeInDown site-intro__helper"><a class="js-smooth-scroll" href="#welcome"><i class="icon icon--arrow-down"></i></a> Scroll down to <br />read more</p>
                </div>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

            </div>
        </div>

    </div>

    <div class="section--divider"></div>    
</section>
