<section id="investing-in-communities" class="js-waypoint4 section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--midnight">
            Investing In Communities
            <div class="toggle">
                <a class="js-smooth-scroll" href="#building-new-homes"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#investing-in-homes"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">Supporting people, across south London, who need a home takes more than providing just bricks and mortar. We’re investing in communities to provide opportunities for residents who want to achieve goals and reach their potential.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <img src="/assets/images/sections/11-Wandle-Investing-in-Communities.jpg" alt="Wandle staff assisting customer" />
                            <a class="js-zoom-trigger image__zoom image__zoom--midnight"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--midnight">
                            Helping our residents stabilise or improve their incomes by training them on valuable business skills.
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/11-Wandle-Investing-in-Communities.jpg" alt="Wandle staff assisting customer" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--midnight"><i class="icon icon--x"></i></a>
                            <figcaption class="js-zoom-trigger image__caption image__caption--large image__caption--midnight">
                                Helping our residents stabilise or improve their incomes by training them on valuable business skills.
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section__inner section__inner--space">
        <div class="container">
            <div class="l-col3">
                <div class="l-col3-col l-col3-col1">

                    <div class="js-waypoint4-up animation-fade block block--midnight block--light-text block--min-height">
                        <img class="block__img" src="/assets/images/communities/residents.svg" />
                        <p class="block__desc">Wandle’s Start Your Own Business courses in 2019. 2 courses ran, training 32 local people, 22 of whom are Wandle residents</p>
                    </div>

                </div>
                <div class="l-col3-col l-col3-col2">

                    <div class="js-waypoint4-up animation-fade animated--delay-02s block block--midnight block--light-text block--min-height">
                        <img class="block__img" src="/assets/images/communities/chart.svg" />
                        <p class="block__desc">13 Love London Working participants went into employment in 2019, with 59 job starts to date since the start of the programme</p>
                    </div>

                </div>
                <div class="l-col3-col l-col3-col3">

                    <div class="js-waypoint4-up animation-fade animated--delay-04s block block--midnight block--light-text block--min-height">
                        <img class="block__img" src="/assets/images/communities/digital.svg" />                        
                        <p class="block__desc">6 Love London Working participants that got a job in 2019 have now been in employment for 6 months or more</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>