<section id="welcome" class="section section--border section--space section--light-grey">

    <div class="split-container split-container--left split-container--aubergine"></div>
    <div class="split-container split-container--right split-container--welcome split-container--welcome-right"></div>

    <div class="container">

        <h2 class="title title--h2 title--purple">
            Welcome
            <div class="toggle">
                <a class="js-smooth-scroll" href="#home"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#purpose"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article article--light-text">
                    <p class="article__intro">This year we have developed a Target Operating Model, describing how Wandle will operate once we’ve delivered our five year transformation programme. It sets out our commitment for better, more efficient services which offer greater value for you.</p>
                    <p>Our customer satisfaction results during the year were disappointing, with overall tenant satisfaction rated at 61%. Our target is to improve this significantly over the next five years, with next year’s target at 68%. You can track our progress online, where you can see our full Corporate Plan.</p>
                    <p>The new Target Operating Model and Corporate Plan aims to significantly improve our services and the outcomes we achieve for you, and improve our value for money. It builds on our IT programme which is well underway, and will include a portal for you to access our services - which will be co-designed with customers. </p>
                    <p>We have begun a Managed Service Partnership with Mears, initially focused on day-to-day repairs, with the aim to improve our service for you. We are at the early stages of our journey and expect it to take some time before all the identified improvements are set in. However, we have made good progress and will continue to make further improvements to our services.</p>
                    <p>During the year, the Regulator for Social Housing carried out an in-depth assessment on Wandle and changed our governance rating from G1 to G2 – which remains a compliant rating. We have, and continue to work closely with them to progress our action plan. The Board are committed to returning to a G1 rating as soon as possible.</p>
                    <p>We hope that you enjoy reading more about our progress over the past year and a glimpse at our commitments for next year.</p>

                    <div class="l-article-cols">
                        <div class="l-article-cols-col l-article-cols-col1">
                            <img class="article__image article__image--left" src="/assets/images/sections/Wandle-Tom-Bremner.jpg" alt="Tom Bremner">
                            <img class="article__image article__image--right" src="/assets/images/sections/Wandle-Tracey-Lees.jpg" alt="Tracey Lees">
                        </div>
                        <div class="l-article-cols-col l-article-cols-col2">
                            <p>Tracey Lees, Chief Executive</p>
                            <p>Tom Bremner, Chair</p>
                        </div>
                    </div>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

            </div>
        </div>

    </div>

    <div class="section--divider"></div>
</section>
