<section id="our-corporate-plan" class="section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--blood">
            Our corporate plan
            <div class="toggle">
                <a class="js-smooth-scroll" href="#in-numbers"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#value-for-money"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">2018/19 has been a year of laying foundations for our new corporate plan, which launched in April 2019.</p>
                    <p>Our corporate plan responded to the changing economic and political environment that we’re operating in, while striving to deliver quality, responsive and effective services.</p>
                    <p>We focussed on Value, People, Homes, Places and Innovation, and developing our new plan to 2025.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img image__img--no-caption">
                            <img src="/assets/images/articles/Corporate-Priorities-poster.png" alt="Corporate Plan Illustration" />
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>