<section id="in-numbers" class="js-waypoint1 section section--border section--space">
    <div class="container">

        <h2 class="title title--h2 title--sky">
            2018/2019 In Numbers
            <div class="toggle">
                <a class="js-smooth-scroll" href="#our-homes"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#road"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2 l-col2--early-break">
            <div class="l-col2-col l-col2-col--early-break l-col2-col1">

                <div class="js-waypoint1-left animation-fade block block--sky block--light-text block--has-foot">
                    <div class="l-block l-block1">
                        <img class="block__symbol" src="/assets/images/blocks/houses.svg" />
                        &nbsp;
                    </div>
                    <div class="l-block l-block2">
                        <h3 class="block__title">Total Cost Per Social Home: <br /><strong>£4,804</strong></h3>
                        <p class="block__detail">2017/18: £4,812</p>
                    </div>
                </div>

            </div>
            <div class="l-col2-col l-col2-col--early-break l-col2-col2">

                <div class="js-waypoint1-right animation-fade block block--sky block--light-text block--has-foot">
                    <div class="l-block l-block1">
                        <img class="block__symbol" src="/assets/images/blocks/maze.svg" />
                        &nbsp;
                    </div>
                    <div class="l-block l-block2">
                        <h3 class="block__title">Operating Margin: <strong>28.4%</strong></h3>
                        <p class="block__detail">(Social Housing) 17/18: <b>29.7%</b></p>
                        <p class="block__detail"><br />This is a key ratio for demonstrating that Wandle is operating within its means.</p>
                    </div>
                </div>

            </div>
        </div>

        <div class="l-col2 l-col2--early-break">
            <div class="l-col2-col l-col2-col--early-break l-col2-col1">

                <div class="js-waypoint1-left animation-fade block block--sky block--light-text block--has-foot">
                    <div class="l-block l-block1">
                        <img class="block__symbol" src="/assets/images/blocks/turnover.svg" />
                        &nbsp;
                    </div>
                    <div class="l-block l-block2">
                        <h3 class="block__title">Turnover: <br /><strong>&pound;68.6 million</strong></h3>
                        <h3 class="block__title">2017/18: <br /><strong>&pound;69.9 million</strong></h3>
                    </div>
                </div>

            </div>
            <div class="l-col2-col l-col2-col--early-break l-col2-col2">

                <div class="js-waypoint1-right animation-fade block block--sky block--light-text block--has-foot">
                    <div class="l-block l-block1">
                        <img class="block__symbol" src="/assets/images/blocks/piggy.svg" />
                        &nbsp;
                    </div>
                    <div class="l-block l-block2">
                        <h3 class="block__title">Overall Surplus: <strong>&pound;18.8 million</strong></h3>
                        <p class="block__detail">2017/18: <b>&pound;20.8m</b></p>
                        <p class="block__detail">Any surplus generated on landlord activities contributes towards the development of affordable homes.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>
