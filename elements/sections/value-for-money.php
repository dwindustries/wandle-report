<section id="value-for-money" class="section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--tangerine">
            Value For Money
            <div class="toggle">
                <a class="js-smooth-scroll" href="#road"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#improving-customer-experience"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                  <p class="article__intro">Wandle strives to achieve value for money so that we may provide more for our customers in terms of service and new homes.</p>
                  <p>The operating environment has been challenging for us to deliver this, but we remain on target to deliver financial strength and our planned efficiency savings.</p>
                  <p>As an organisation we have efficiency savings targets of £4.4m to be achieved by 2025.</p>
                  <p>We are on target to deliver these and now that the early stages of the Stream (digitalisation) programme are starting to be delivered, the Board will be considering the next steps in terms of driving further value for money to achieve more for our customers. This will be considered at a strategic level and will be part of the targets we set ourselves for 2020 onwards. <a href="https://www.wandle.com/download.cfm?doc=docm93jijm4n1898.pdf&ver=2406" target="_blank">Our full strategic report and financial statements are available to read here.</a></p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <a href="https://www.wandle.com/download.cfm?doc=docm93jijm4n1898.pdf&ver=2406" target="_blank"><img src="/assets/images/sections/8-Wandle-Value-for-Money.jpg" alt="Wandle Staff looking at plans" /></a>
                            <a href="https://www.wandle.com/download.cfm?doc=docm93jijm4n1898.pdf&ver=2406" class="image__zoom image__zoom--tangerine" target="_blank"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--tangerine">
                          <a href="https://www.wandle.com/download.cfm?doc=docm93jijm4n1898.pdf&ver=2406" target="_blank">Better insights and planning have delivered more efficient services.</a>
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/8-Wandle-Value-for-Money.jpg" alt="Wandle Staff looking at plans" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--tangerine"><i class="icon icon--x"></i></a>
                            <figcaption class="js-zoom-trigger image__caption image__caption--large image__caption--tangerine">
                                Better insights and planning have delivered more efficient services.
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>