<section id="purpose" class="js-waypoint0 section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--grass">
            Our purpose, vision and values
            <div class="toggle">
                <a class="js-smooth-scroll" href="#welcome"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#our-homes"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">Our purpose, vision and values guide everything we do at Wandle.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <img src="/assets/images/sections/5-Wandle-Our-Purpose-Visions-Values.jpg" alt="Staff celebrate awards" />
                            <a class="js-zoom-trigger image__zoom image__zoom--grass"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--grass">
                          Our staff awards celebrate colleagues that demonstrated our corporate values every day at work.
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/5-Wandle-Our-Purpose-Visions-Values.jpg" alt="Staff celebrate awards" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--grass"><i class="icon icon--x"></i></a>
                            <figcaption class="image__caption image__caption--large image__caption--grass">
                              Our staff awards celebrate colleagues that demonstrated our corporate values every day at work.
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section__inner section__inner--push-down">
        <div class="container">
            <div class="l-col2">
                <div class="l-col2-col l-col2-col--even l-col2-col1">

                    <h3 class="title title--h3 title--grass title--pull-up">Our Vision</h3>
                    <div class="js-waypoint0-left animation-fade article article--push-down article--space">
                        <div class="l-col2">
                            <div class="l-col2-col l-col2-col1">
                                <img class="purpose-img" src="/assets/images/articles/map.svg" alt="Map">
                            </div>
                            <div class="l-col2-col l-col2-col2">
                                <div class="purpose-text purpose-text1">
                                    <p><strong>Homes to be proud of and services you can trust</strong></p>
                                </div>
                            </div>
                        </div> 
                    </div>             

                </div>
                <div class="l-col2-col l-col2-col--even l-col2-col2">

                    <h3 class="title title--h3 title--grass title--pull-up">Our Purpose</h3>
                    <div class="js-waypoint0-right animation-fade article article--push-down article--space">
                        <div class="l-col2">
                            <div class="l-col2-col l-col2-col1">
                                <img class="purpose-img" src="/assets/images/articles/target.svg" alt="Map">
                            </div>
                            <div class="l-col2-col l-col2-col2">

                                <div class="purpose-text">
                                    <p class="text--no-space"><strong>Supporting people, across south London, who need a home</strong></p>
                                    <ul>
                                        <li>Reliable customer service</li>
                                        <li>Building and managing good quality, affordable homes</li>
                                        <li>Investing in local communities</li>
                                        <li>Attracting and nurturing the best people</li>
                                    </ul>
                                </div>

                            </div>
                        </div> 
                    </div>  

                </div>
            </div>

            <h3 class="title title--h3 title--grass">Our Values</h3>

            <div class="js-waypoint00 values">
                <div class="l-value">

                    <div class="js-waypoint00-up animation-fade value">
                        <img class="value__symbol" src="assets/images/values/customers.svg" alt="Customer Image" />
                        <h4 class="value__title">Think customer</h4>
                        <p>We believe the experience of our customers is everything, and that will always be at the forefront of our minds</p>
                    </div>

                </div>
                <div class="l-value">

                    <div class="js-waypoint00-up animation-fade animated--delay-02s value">
                        <img class="value__symbol" src="assets/images/values/handshake.svg" alt="Customer Image" />
                        <h4 class="value__title">Build relationships</h4>
                        <p>We recognise that every person is unique, and reﬂect this in the relationships we build</p>
                    </div>

                </div>
                <div class="l-value">

                    <div class="js-waypoint00-up animation-fade animated--delay-04s value">
                        <img class="value__symbol" src="assets/images/values/together.svg" alt="Customer Image" />
                        <h4 class="value__title">Work together</h4>
                        <p>We share insight, knowledge and experience to improve the way we work</p>
                    </div>

                </div>
                <div class="l-value">

                    <div class="js-waypoint00-up animation-fade animated--delay-06s value">
                        <img class="value__symbol" src="assets/images/values/rocket.svg" alt="Customer Image" />
                        <h4 class="value__title">Aim High</h4>
                        <p>We are ambitious for ourselves and each other, and celebrate and share in success</p>
                    </div>

                </div>
                <div class="l-value">

                    <div class="js-waypoint00-up animation-fade animated--delay-08s value">
                        <img class="value__symbol" src="assets/images/values/badge.svg" alt="Customer Image" />
                        <h4 class="value__title">Own it</h4>
                        <p>We take individual and collective responsibility for our actions, and are honest when things don’t go right</p>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="section--divider"></div>
</section>