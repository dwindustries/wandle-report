<section id="building-new-homes" class="js-waypoint3 section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--gold">
            Building New Homes
            <div class="toggle">
                <a class="js-smooth-scroll" href="#improving-customer-experience"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#investing-in-communities"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">Wandle helps south Londoners who need a home and as a developing housing association, we’re helping to tackle the shortage of good quality affordable housing in the capital.</p> 
                    <p>Our development programme is providing new homes both for the people most in need of housing, and those who would otherwise not be able to afford to own their own home.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <img src="/assets/images/sections/10-Wandle-Building-New-Homes.jpg" alt="Board room going over plans" />
                            <a class="js-zoom-trigger image__zoom image__zoom--gold"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--gold">
                            At Albany House, 42 new homes will be available for shared ownership and rent in spring 2020
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/10-Wandle-Building-New-Homes.jpg" alt="Board room going over plans" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--gold"><i class="icon icon--x"></i></a>
                            <figcaption class="js-zoom-trigger image__caption image__caption--large image__caption--gold">
                                At Albany House, 42 new homes will be available for shared ownership and rent in spring 2020
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section__inner section__inner--space">
        <div class="container">
            <div class="l-col3">
                <div class="l-col3-col l-col3-col1">

                    <div class="js-waypoint3-up animation-fade block block--gold block--light-text block--min-height">
                        <h3 class="block__title block__title--large">128 New Homes</h3>
                        <p class="block__desc">We have continued to provide more homes with 128 new homes completed this year.</p>
                    </div>

                </div>
                <div class="l-col3-col l-col3-col2">

                    <div class="js-waypoint3-up animation-fade animated--delay-02s block block--gold block--light-text block--min-height">
                        <h3 class="block__title block__title--large">Homes people want to own</h3>
                        <p class="block__desc">We have continued to build more shared ownership homes, helping generation rent become generation buy.</p>
                    </div>

                </div>
                <div class="l-col3-col l-col3-col3">

                    <div class="js-waypoint3-up animation-fade animated--delay-04s block block--gold block--light-text block--min-height">
                        <h3 class="block__title block__title--large">Setting the bar</h3>
                        <p class="block__desc">Our New Homes Design Brief defines the quality, design and specification all our new homes are built to. This brief provides clarity to our construction partners, and helps us avoid poor design.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>