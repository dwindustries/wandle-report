<section id="investing-in-repairs" class="js-waypoint6 section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--orange">
            Investing In Repairs
            <div class="toggle">
                <a class="js-smooth-scroll" href="#investing-in-homes"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#our-people"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">We have commenced a Managed Service Partnership with Mears, initially focused on day-to-day repairs with the aim to improve the service for residents and drive our efficiencies. We are at the early stages of our transformational journey and expect it to take some time before all the identified improvements are embedded. However we have made good progress and will continue to make further improvements to our services.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <img src="/assets/images/sections/13-Wandle-Investing-in-Repairs.jpg" alt="Repairing a hinge" />
                            <a class="js-zoom-trigger image__zoom image__zoom--orange"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--orange">
                            Repairs are the number one driver for resident satisfaction.
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/13-Wandle-Investing-in-Repairs.jpg" alt="Repairing a hinge" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--orange"><i class="icon icon--x"></i></a>
                            <figcaption class="image__caption image__caption--large image__caption--orange">
                                Repairs are the number one driver for customer satisfaction.
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section__inner section__inner--push-down">
        <div class="container">
            <div class="l-col2">
                <div class="l-col2-col l-col2-col--even l-col2-col1">

                    <h3 class="title title--h3 title--orange title--pull-up">Jobs Per Day</h3>
                    <div class="js-waypoint6-left animation-fade article article--push-down article--space">
                        <div class="l-col2">
                            <div class="l-col2-col l-col2-col1">
                                <img class="repair-img" src="/assets/images/repairs/heart.svg" alt="Heart Pulse">
                            </div>
                            <div class="l-col2-col l-col2-col2">
                                <div class="repair-text repair-text1">
                                    <p><strong>On average, we completed over 50 repairs every day in 2018/19</strong></p>
                                </div>
                            </div>
                        </div> 
                    </div>             

                </div>
                <div class="l-col2-col l-col2-col--even l-col2-col2">

                    <h3 class="title title--h3 title--orange title--pull-up">Satisfaction</h3>
                    <div class="js-waypoint6-left animation-fade animated--delay-02s article article--push-down article--space">
                        <div class="js-waypoint6-left animation-fade section__inner__divider section__inner__divider--chevron">
                            <i class="icon icon--chevron"></i>
                        </div>

                        <div class="l-col2">
                            <div class="l-col2-col l-col2-col1">
                                <img class="repair-img repair-img2" src="/assets/images/repairs/workman.svg" alt="Workman">
                            </div>
                            <div class="l-col2-col l-col2-col2">

                                <div class="repair-text repair-text1">
                                    <p><strong>Satisfaction with repairs completed was at 99.61% in quarter 4</strong></p>
                                </div>

                            </div>
                        </div> 
                    </div>  

                </div>
            </div>

        </div>
    </div>

    <div class="section--divider"></div>
</section>