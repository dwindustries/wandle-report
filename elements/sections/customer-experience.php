<section id="improving-customer-experience" class="js-waypoint2 section section--border section--space section--light-grey">
    <div class="split-container split-container--left split-container--white"></div>
    <div class="split-container split-container--right"></div>

    <div class="container">
        <h2 class="title title--h2 title--azure">
            Improving Customer Experience
            <div class="toggle">
                <a class="js-smooth-scroll" href="#value-for-money"><i class="icon icon--arrow icon--arrow-up"></i></a>
                <a class="js-smooth-scroll" href="#building-new-homes"><i class="icon icon--arrow icon--arrow-down"></i></a>
            </div>
        </h2>

        <div class="l-col2">
            <div class="l-col2-col l-col2-col--even l-col2-col1">

                <article class="article">
                    <p class="article__intro">Our customer satisfaction results during the year were disappointing, with overall tenant satisfaction rated as 61%. The new Corporate Plan aims to significantly improve our services and the outcomes we achieve for our residents.</p>
                    <p>We are working to improve satisfaction over the next five years, with our target for 2020 being 68%. We hope to achieve this by implementing improvements in a number of areas, with our corporate priorities responding to the UK’s changing economic and political environment. Next year will see the development of customer experience and resident involvement strategies, which will set out clear guidance for the organisation and more effective resident scrutiny.</p>
                </article>

            </div>
            <div class="l-col2-col l-col2-col--even l-col2-col2">

                <div class="js-zoom">
                    <figure class="image">
                        <div class="image__img">
                            <a href="http://strategy.wandle.com/" target="_blank"><img src="/assets/images/sections/9-Wandle-Improving-Customer-Experience.jpg" alt="Customer unlocking front door" /></a>
                            <a class="image__zoom image__zoom--azure" href="http://strategy.wandle.com/" target="_blank"><i class="icon icon--magnifying-glass"></i></a>
                        </div>
                        <figcaption class="image__caption image__caption--azure">
                        Our new corporate plan aims to significantly improve our residents experience.
                        </figcaption>
                    </figure>
                    <figure class="image image--large">
                        <div class="image__img image__img--large">
                            <img src="/assets/images/sections/9-Wandle-Improving-Customer-Experience.jpg" alt="Customer unlocking front door" />
                            <a class="js-zoom-trigger image__zoom image__zoom--large image__zoom--azure"><i class="icon icon--x"></i></a>
                            <figcaption class="js-zoom-trigger image__caption image__caption--large image__caption--azure">
                              Our new corporate plan aims to significantly improve our residents experience.
                            </figcaption>
                        </div>
                    </figure>
                </div>

            </div>
        </div>
    </div>

    <div class="section--divider"></div>
</section>