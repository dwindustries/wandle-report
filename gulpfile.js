var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	sass = require('gulp-ruby-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	livereload = require('gulp-livereload'),
	imageop = require('gulp-image-optimization'),
	svgSprite = require('gulp-svg-sprite'),
	svg2png = require('gulp-svg2png'),
	svgo = require('gulp-svgo'),
	gutil = require('gulp-util'),
	vinyl = require('vinyl'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload;

// Paths
var assets_path = './assets';
var basePaths = {
	src: assets_path,
	dest: assets_path+'/build',
};
var paths = {
	images: {
		src: basePaths.src + '/images/',
		dest: basePaths.dest + '/images/pngs/'
	},
	sprite: {
		src: basePaths.src + '/images/svgs/*',
		svg: basePaths.dest + '/images/svgs/sprite.svg',
		css: basePaths.src + '/scss/_sprite.scss'
	},
	templates: {
		src: basePaths.src + '/build/tpl/'
	}
};


// Static server
gulp.task('browser-sync', function() {
    //initialize browsersync
    browserSync({
        proxy: "wandle.l"
    });
});

function errorLog(error) {
	console.error.bind(error);
	this.emit('end');
}

// Scripts
// uglifies and concatenates js
gulp.task('scripts', function() {
	gulp.src([
		assets_path+'/js/libs/jquery-1.10.1.min.js',
		assets_path+'/js/libs/jquery.counterup.min.js',
		assets_path+'/js/libs/jquery.waypoints.min.js',
		assets_path+'/js/global.js'
	])
	.pipe(plumber())
	.pipe(uglify())
	.pipe(concat('all.js'))
	.pipe(gulp.dest(assets_path+'/build/js/'));
});

// Styles
// uglifies, uses plumber for errors and live reload
gulp.task('styles', function() {
	return sass(assets_path+'/scss/all.scss', { 
		style: 'nested', 
		sourcemap: true 
	})
	.on('error', function (err) {
		console.error('Error', err.message);
	})
    .pipe(sourcemaps.write('maps', {
        includeContent: false,
        sourceRoot: '/source'
    }))
    .pipe(gulp.dest(assets_path+'/build/css/'))
	.pipe(livereload());
});

// SVG Sprite
// Create an SVG sprite wth png fallbacks
gulp.task('svgSprite', function () {

	return gulp.src(paths.sprite.src)
		.pipe(svgo())
		.pipe(svgSprite({
			"mode": {
				"css": {
					"spacing": {
						"padding": 5
					},
					"dest": "./",
					"layout": "diagonal",
					"sprite": "../../assets/build/images/svgs/sprite.svg",
					"bust": false,
					"render": {
						"scss": {
							"dest": "../../assets/scss/_sprite.scss",
							"template": assets_path + "/build/tpl/sprite-template.scss"
						}
					}
				}
			}
		}))
		.pipe(gulp.dest(basePaths.dest));

});

gulp.task('pngSprite', ['svgSprite'], function() {
	return gulp.src(assets_path+"/build/images/svgs/sprite.svg")
		.pipe(svg2png())
		.pipe(gulp.dest(assets_path+"/build/images/pngs/"));
});

gulp.task('sprite', ['pngSprite']);


// Images
// optimise images
gulp.task('images', function(cb) {
    gulp.src([
    	assets_path+'/images/**/*.png',
    	assets_path+'/images/**/*.jpg',
    	assets_path+'/images/**/*.gif',
    	assets_path+'/images/**/*.jpeg'
    ])
    .pipe(imageop({
        optimizationLevel: 8,
        progressive: true,
        interlaced: true
    }));
});


// Watch
gulp.task('watch', function() {

	livereload.listen();
	gulp.watch(assets_path+'/js/**/*.js',['scripts']);
	gulp.watch(assets_path+'/scss/**/*.scss',['styles']);
    gulp.watch('./**/*.php').on('change', livereload.changed);

});

// gulp Default
gulp.task('default', ['styles','scripts','svgSprite','pngSprite','images','browser-sync','watch']);