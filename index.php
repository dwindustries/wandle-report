<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Wandle | Annual Report 2018/1019</title>
        <meta type="name" content="Over the last twelve months, Wandle has taken great strides forward; transforming the way we work and laying the foundations for future success." />
        <?php include 'elements/site/head.php'; ?>
    </head>

    <body>

        <div class="overlay-background"></div>

        <?php include 'elements/site/header.php'; ?>
    
        <div class="page">

            <?php include 'elements/sections/home.php'; ?>
            <?php include 'elements/sections/welcome.php'; ?>
            <?php include 'elements/sections/purpose.php'; ?>
            <?php include 'elements/sections/our-homes.php'; ?>
            <?php include 'elements/sections/in-numbers.php'; ?>
            <?php include 'elements/sections/corporate-plan.php'; ?>
            <?php include 'elements/sections/value-for-money.php'; ?>
            <?php include 'elements/sections/customer-experience.php'; ?>
            <?php include 'elements/sections/building-new-homes.php'; ?>
            <?php include 'elements/sections/investing-in-communities.php'; ?>
            <?php include 'elements/sections/investing-in-homes.php'; ?>
            <?php include 'elements/sections/investing-in-repairs.php'; ?>
            <?php include 'elements/sections/our-people.php'; ?>
            <?php include 'elements/sections/strengthening-our-governance.php'; ?>

        </div>

        <?php include 'elements/site/footer.php'; ?>
        <?php include 'elements/site/js.php'; ?>

    </body>
</html>