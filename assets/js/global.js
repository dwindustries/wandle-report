function nav() {
    var $nav_link = $('.js-menu-link'),
        $nav_items = $('.js-menu-items');

    $nav_items.hide();

    $nav_link.click(function() {
        if(!$nav_items.hasClass('active')) {
          $nav_link.addClass('active');
          $nav_items.addClass('active');
          $nav_items.slideDown();
        }
        else {
          $nav_link.removeClass('active');
          $nav_items.removeClass('active');
          $nav_items.slideUp();
        }
        return false;
    });
}


function zoom(){
  var trigger = $('.js-zoom-trigger');

  $(trigger).on('click', function(){
    var img = $(this).closest('.js-zoom').find('.image--large');

    $(img).fadeToggle();
    $('.overlay-background').fadeToggle();
  });

  $('.overlay-background').on('click', function(){
    $('.js-zoom .image--large').fadeOut();
    $('.overlay-background').fadeOut();
  });

  $(document).keyup(function(e) {
    if (e.keyCode === 27){
      $('.js-zoom .image--large').fadeOut();
      $('.overlay-background').fadeOut();
    }
  });
}


// Smooth scrolling anchor links
function scrollAnimate() {
  var $smoothScroll = $('.js-smooth-scroll');

  $smoothScroll.click(function() {
      // Close Nav
      $('.js-menu-link').removeClass('active');
      $('.js-menu-items').slideUp().removeClass('active');

      // Smooth scroll to item
      if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') || location.hostname === this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
             if (target.length) {
               $('html,body').animate({
                   scrollTop: target.offset().top
              }, 1000);
              return false;
          }
      }
  });
}


function counter() {
  var counterUp = window.counterUp["default"]; // import counterUp from "counterup2"
  var $counters = $(".js-counter");

    /* Start counting, do this on DOM ready or with Waypoints. */
  $counters.each(function (ignore, counter) {
    var waypoint = new Waypoint( {
      element: $(this),
      handler: function() { 
        counterUp(counter, {
          duration: 5000,
          delay: 16
        }); 
        this.destroy();
      },
      offset: 'bottom-in-view',
    } );
  });
}


function waypoints() {
 $('.animation-fade').css('opacity','0');

  $('.js-waypoint0').waypoint(function() {
    $('.js-waypoint0-left').addClass('animated fadeInLeft');  
    $('.js-waypoint0-right').addClass('animated fadeInRight');  
  }, {
    offset: '10%'
  });
  $('.js-waypoint00').waypoint(function() {
    $('.js-waypoint00-up').addClass('animated fadeInUp');  
  }, {
    offset: '80%'
  });
  $('.js-waypoint1').waypoint(function() {
    $('.js-waypoint1-left').addClass('animated fadeInLeft');  
    $('.js-waypoint1-right').addClass('animated fadeInRight');  
  }, {
    offset: '10%'
  });
  $('.js-waypoint2').waypoint(function() {
    $('.js-waypoint2-left').addClass('animated fadeInLeft');  
    $('.js-waypoint2-right').addClass('animated fadeInRight');  
  }, {
    offset: '10%'
  });
  $('.js-waypoint3').waypoint(function() {
    $('.js-waypoint3-up').addClass('animated fadeInUp');  
  }, {
    offset: '10%'
  });
  $('.js-waypoint4').waypoint(function() {
    $('.js-waypoint4-up').addClass('animated fadeInUp');  
  }, {
    offset: '10%'
  });
  $('.js-waypoint5').waypoint(function() {
    $('.js-waypoint5-up').addClass('animated fadeInUp');  
  }, {
    offset: '10%'
  });
  $('.js-waypoint6').waypoint(function() {
    $('.js-waypoint6-left').addClass('animated fadeInLeft');  
  }, {
    offset: '10%'
  });
  $('.js-waypoint7').waypoint(function() {
    $('.js-waypoint7-up').addClass('animated fadeInUp');  
  }, {
    offset: '10%'
  });
  $('.js-waypoint8').waypoint(function() {
    $('.js-waypoint8-up').addClass('animated fadeInUp');  
  }, {
    offset: '10%'
  });  
}


$(document).ready(function() {

  if ( $('.js-menu-items').length ) { nav(); }
  if ( $('.js-zoom').length ) { zoom(); }
  if ( $('.js-smooth-scroll').length ) { scrollAnimate(); }
  waypoints();
  counter();

});